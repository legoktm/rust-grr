// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2020-2021, 2024 Kunal Mehta <legoktm@debian.org>
use anyhow::{anyhow, Result};
use clap::{Parser, Subcommand};
use gitreview::parse_gitreview;
use gitreview::DotGitReview;
use log::debug;
use std::fs;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::process::{ExitCode, Stdio};
use std::{env, process};

mod gitreview;
mod http;

const VERSION: &str = env!("CARGO_PKG_VERSION");

fn shell_call(program: &str, args: Vec<String>) -> u8 {
    debug!("{} {}", program, args.join(" "));
    let status = process::Command::new(program)
        .args(&args)
        .stdout(Stdio::piped())
        .status()
        .expect("failed to execute process")
        .code()
        .unwrap_or(1);
    debug!("finished with status {}", status);
    // Windows apparently returns i32 instead of u8, so we might need to squeeze it in
    status.try_into().unwrap_or(1)
}

fn shell_output(program: &str, args: Vec<String>) -> Result<String> {
    debug!("{} {}", program, args.join(" "));
    let output = process::Command::new(program).args(&args).output()?;
    if output.status.code().unwrap_or(1) > 0 {
        return Err(anyhow!("command failed"));
    }
    Ok(String::from_utf8(output.stdout)?)
}

fn review(branch: &str, extra: &str) -> u8 {
    let to = format!("HEAD:refs/for/{}{}", branch, extra);
    shell_call("git", vec!["push".to_string(), "origin".to_string(), to])
}

fn rebase(branch: &str) -> u8 {
    let target = format!("origin/{}", branch);
    shell_call("git", vec!["rebase".to_string(), target])
}

fn pull(branch: &str, should_rebase: bool) -> u8 {
    let code =
        shell_call("git", vec!["fetch".to_string(), "origin".to_string()]);
    if code != 0 {
        return code;
    }
    if should_rebase {
        rebase(branch)
    } else {
        checkout(branch)
    }
}

fn checkout(branch: &str) -> u8 {
    let target = format!("origin/{}", branch);
    shell_call("git", vec!["checkout".to_string(), target])
}

fn fetch_patch(patch: &str, cfg: DotGitReview) -> Result<u8> {
    let info = http::fetch_patch_info(patch, cfg)?;
    Ok(shell_call(
        "git",
        vec!["fetch".to_string(), info.url, info.git_ref],
    ))
}

fn fetch(patch: &str, config: DotGitReview) -> Result<u8> {
    fetch_patch(patch, config)?;
    Ok(shell_call(
        "git",
        vec!["checkout".to_string(), "FETCH_HEAD".to_string()],
    ))
}

fn cherry_pick(patch: &str, config: DotGitReview) -> Result<u8> {
    fetch_patch(patch, config)?;
    Ok(shell_call(
        "git",
        vec!["cherry-pick".to_string(), "FETCH_HEAD".to_string()],
    ))
}

fn init(force: bool) -> Result<u8> {
    let commit_msg = include_str!("commit-msg");
    let rev_parse = shell_output(
        "git",
        vec![
            "rev-parse".to_string(),
            "--path-format=absolute".to_string(),
            "--git-path".to_string(),
            "hooks/commit-msg".to_string(),
        ],
    )?
    .trim()
    .to_string();
    let fpath = Path::new(&rev_parse);

    if !force && fpath.exists() {
        // Already exists
        println!("A commit-msg hook is already configured.");
        return Ok(0);
    }

    let mut file = fs::File::create(fpath)?;
    file.write_all(commit_msg.as_bytes())?;

    // Set the file to executable
    // TODO: find a better way to do this
    let code = shell_call(
        "chmod",
        vec!["+x".to_string(), fpath.to_str().unwrap().to_string()],
    );
    if code == 0 {
        println!("Installed commit-msg hook.")
    }
    Ok(code)
}

/// Set review as the default action
fn fix_args(original_args: Vec<String>) -> Vec<String> {
    let mut args = original_args;
    // Would be nice if we didn't have to hardcode this
    let subcommands = [
        "review",
        "pull",
        "rebase",
        "checkout",
        "fetch",
        "cherry-pick",
        "init",
        // added by clap
        "help",
    ];
    if args.contains(&"--help".to_string())
        || args.contains(&"--version".to_string())
    {
        // Do nothing
    } else if args.len() < 2 {
        args.push("review".to_string());
    } else if !subcommands.contains(&args[1].as_str()) {
        args.insert(1, "review".to_string());
    }

    args
}

fn build_review(
    topic: Option<&String>,
    code_review: Option<&String>,
    verified: Option<&String>,
    submit: bool,
    hashtags: Option<&String>,
) -> String {
    let mut extra = vec![];
    if let Some(topic) = topic {
        extra.push(format!("topic={}", topic));
    }
    if let Some(code_review) = code_review {
        // TODO: validate
        extra.push(format!("l=Code-Review{}", code_review));
    }
    if let Some(verified) = verified {
        // TODO: validate
        extra.push(format!("l=Verified{}", verified));
    }
    if let Some(hashtags) = hashtags {
        extra.push(format!("t={}", hashtags));
    }
    if submit {
        extra.push("submit".to_string());
    }

    format!("%{}", extra.join(","))
}

fn workdir() -> PathBuf {
    let dir = shell_output(
        "git",
        vec!["rev-parse".to_string(), "--show-toplevel".to_string()],
    )
    .expect("Unable to get working directory");
    PathBuf::from(dir.trim())
}

fn default_branch() -> String {
    let result = shell_output(
        "git",
        vec![
            "config".to_string(),
            "--get".to_string(),
            "grr.defaultBranch".to_string(),
        ],
    );

    match result {
        Ok(branch) => branch.trim().to_string(),
        Err(_) => "master".to_string(),
    }
}

#[derive(Parser)]
#[command(version, about)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    #[command(about = "Upload a patch for review")]
    Review {
        #[arg(help = "Submit for this branch")]
        branch: Option<String>,
        #[arg(short, long, help = "Topic to set")]
        topic: Option<String>,
        #[arg(short, long, help = "Code-Review value to set (-2 to +2)")]
        code_review: Option<String>,
        #[arg(short, long, help = "Verified value to set (-2 to +2)")]
        verified: Option<String>,
        #[arg(
            short,
            long,
            help = "If the patch should be submitted after uploading"
        )]
        submit: bool,
        // short -h would conflict with builtin --help
        #[arg(long, help = "Comma-separated list of hashtags to set")]
        hashtags: Option<String>,
    },

    #[command(about = "Pull from remote")]
    Pull {
        #[arg(help = "Branch to pull from")]
        branch: Option<String>,
        #[arg(long, help = "Rebase instead of checkout")]
        rebase: bool,
    },

    #[command(about = "Checkout a remote branch")]
    Checkout {
        #[arg(help = "Branch to checkout")]
        branch: Option<String>,
    },

    #[command(about = "Rebase on top of a remote branch")]
    Rebase {
        #[arg(help = "Branch to rebase on")]
        branch: Option<String>,
    },

    #[command(about = "Cherry-pick a patch from Gerrit")]
    CherryPick {
        #[arg(help = "Patch to cherry-pick")]
        patch: String,
    },

    #[command(about = "Fetch a patch from Gerrit")]
    Fetch {
        #[arg(help = "Patch to fetch")]
        patch: String,
    },

    #[command(about = "Initialize a new repository")]
    Init {
        #[arg(long, help = "Force initialization")]
        force: bool,
    },
}

fn main() -> Result<ExitCode> {
    env_logger::Builder::from_env(
        env_logger::Env::default().default_filter_or("info"),
    )
    .init();

    let args: Vec<String> = fix_args(env::args().collect());
    let cli = Cli::parse_from(args);
    let status = match cli.command {
        Commands::Review {
            branch,
            topic,
            code_review,
            verified,
            submit,
            hashtags,
        } => {
            let extra = build_review(
                topic.as_ref(),
                code_review.as_ref(),
                verified.as_ref(),
                submit,
                hashtags.as_ref(),
            );
            Ok(review(&branch.unwrap_or_else(default_branch), &extra))
        }
        Commands::Pull { branch, rebase } => {
            Ok(pull(&branch.unwrap_or_else(default_branch), rebase))
        }
        Commands::Rebase { branch } => {
            Ok(rebase(&branch.unwrap_or_else(default_branch)))
        }
        Commands::Checkout { branch } => {
            Ok(checkout(&branch.unwrap_or_else(default_branch)))
        }
        Commands::Fetch { patch } => {
            fetch(&patch, parse_gitreview(workdir().join(".gitreview"))?)
        }
        Commands::CherryPick { patch } => {
            cherry_pick(&patch, parse_gitreview(workdir().join(".gitreview"))?)
        }
        Commands::Init { force } => init(force),
    };
    match status {
        Ok(code) => Ok(ExitCode::from(code)),
        Err(error) => Err(error),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use tempfile::tempdir;

    #[test]
    fn test_fix_args() {
        assert_eq!(
            vec![
                "grr".to_string(),
                "review".to_string(),
                "master".to_string()
            ],
            fix_args(vec!["grr".to_string(), "master".to_string()])
        );
        assert_eq!(
            vec![
                "grr".to_string(),
                "cherry-pick".to_string(),
                "12345".to_string()
            ],
            fix_args(vec![
                "grr".to_string(),
                "cherry-pick".to_string(),
                "12345".to_string()
            ])
        );
        assert_eq!(
            vec!["grr".to_string(), "review".to_string(),],
            fix_args(vec!["grr".to_string()])
        );
        assert_eq!(
            vec!["grr".to_string(), "--version".to_string()],
            fix_args(vec!["grr".to_string(), "--version".to_string()])
        )
    }

    #[test]
    fn test_init() {
        let dir = tempdir().unwrap();
        let current_dir = std::env::current_dir().unwrap();
        git2::Repository::init(dir.path()).unwrap();
        std::env::set_current_dir(dir.path()).unwrap();
        assert_eq!(0, init(false).unwrap());
        let hook = dir.path().join(".git/hooks/commit-msg");
        assert!(hook.exists());
        // Reset
        std::env::set_current_dir(current_dir.as_path()).unwrap();
        // TODO: check executable bit
    }

    #[test]
    fn test_build_review() {
        assert_eq!(
            "%topic=topic,l=Code-Review+2",
            build_review(
                Some(&"topic".to_string()),
                Some(&"+2".to_string()),
                None,
                false,
                None
            )
        );
    }
}
