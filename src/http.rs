// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2020, 2024 Kunal Mehta <legoktm@debian.org>
//! `gerrit-grr` is a command-line utility to work with Gerrit.
use crate::gitreview::DotGitReview;
use anyhow::Result;
use serde::Deserialize;
use std::collections::HashMap;
use ureq::AgentBuilder;

#[derive(Deserialize)]
struct GerritResponse {
    current_revision: String,
    revisions: HashMap<String, GerritRevision>,
}

#[derive(Deserialize)]
struct GerritRevision {
    fetch: AnonymousHttp,
}

#[derive(Deserialize)]
struct AnonymousHttp {
    #[serde(rename = "anonymous http")]
    anonymous_http: Fetch,
}

/// Information needed to fetch a Gerrit patch. Can be used as:
/// `git fetch {url} {git_ref}`.
#[derive(Deserialize, Clone)]
pub(crate) struct Fetch {
    /// URL for remote
    pub(crate) url: String,
    /// refspec to fetch
    #[serde(rename = "ref")]
    pub(crate) git_ref: String,
}

/// Get changes information from Gerrit's API
fn rest_api_changes(host: String, patch: String) -> Result<GerritResponse> {
    let agent = AgentBuilder::new()
        .user_agent(&format!("rust-grr/{}", crate::VERSION))
        .build();
    let resp = agent
        .get(&format!(
            "https://{}/r/changes/{}?o=CURRENT_REVISION",
            host, patch
        ))
        .call()?
        .into_string()?;
    Ok(serde_json::from_str(&resp[4..])?)
}

/// Get information about how to fetch a Gerrit patch. `patch` can either be
/// just the change number (`12345`) or include an optional patchset
/// (`12345:2`).
///
/// # Example
/// ```
/// # use gerrit_grr::{parse_gitreview, fetch_patch_info};
/// # fn run() -> anyhow::Result<()> {
/// let cfg = parse_gitreview(".gitreview")?;
/// let fetch = fetch_patch_info("12345", cfg)?;
/// # Ok(())
/// # }
/// ```
pub(crate) fn fetch_patch_info(
    patch: &str,
    cfg: DotGitReview,
) -> Result<Fetch> {
    if patch.contains(':') {
        let split: Vec<&str> = patch.split(':').collect();
        let change = split[0];
        let patchset = split[1];
        Ok(Fetch {
            url: format!("https://{}/r/{}", cfg.host, cfg.project),
            git_ref: format!(
                "refs/changes/{}/{}/{}",
                &change[3..5],
                &change,
                &patchset
            ),
        })
    } else {
        let resp = rest_api_changes(cfg.host, patch.to_string())?;
        Ok(resp.revisions[&resp.current_revision]
            .fetch
            .anonymous_http
            .clone())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_rest_api() {
        let resp = rest_api_changes(
            "gerrit.wikimedia.org".to_string(),
            "601523".to_string(),
        )
        .unwrap();
        assert_eq!(
            "373d514cc9e7fea5d05b9bf7915b70c02a42c115".to_string(),
            resp.current_revision
        );
        let fetch = resp.revisions[&resp.current_revision]
            .fetch
            .anonymous_http
            .clone();
        assert_eq!(
            "https://gerrit.wikimedia.org/r/labs/tools/newusers".to_string(),
            fetch.url
        );
        assert_eq!("refs/changes/23/601523/1".to_string(), fetch.git_ref);
    }
}
