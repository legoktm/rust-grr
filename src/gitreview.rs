// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2020, 2024 Kunal Mehta <legoktm@debian.org>
use anyhow::{Context, Result};
use ini::Ini;
use log::debug;
use std::path::Path;

/// Represents metadata found in `.gitreview` files
pub(crate) struct DotGitReview {
    /// Hostname of Gerrit instance, e.g. `gerrit.wikimedia.org`
    pub(crate) host: String,
    /// Project name, with no trailing `.git` suffix
    pub(crate) project: String,
}

impl DotGitReview {
    /// Create an instance if you already know the host URL and project name
    pub(crate) fn new(host: &str, project: &str) -> DotGitReview {
        DotGitReview {
            host: host.to_string(),
            project: project.to_string(),
        }
    }
}

pub(crate) fn parse_gitreview<P: AsRef<Path>>(
    filename: P,
) -> Result<DotGitReview> {
    debug!("parsing {}", filename.as_ref().display());
    let conf =
        Ini::load_from_file(filename).context("Failed to load .gitreview")?;
    let section = conf.section(Some("gerrit")).unwrap();
    let host = section.get("host").unwrap().to_string();
    // strip_suffix in the future
    let project = section.get("project").unwrap().replace(".git", "");
    Ok(DotGitReview::new(&host, &project))
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_parse_gitrevew() {
        let result = parse_gitreview("tests/good_gitreview").unwrap();
        assert_eq!("gerrit.example.org", result.host);
        assert_eq!("test/foo", result.project);
    }

    #[test]
    #[should_panic]
    fn test_parse_gitrevew_bad() {
        // Missing project
        parse_gitreview("tests/bad_gitreview").unwrap();
    }
}
