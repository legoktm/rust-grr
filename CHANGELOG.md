## 5.0.1 / 2024-09-24

* Fix `fetch` and `cherry-pick` commands

## 5.0.0 / 2024-08-09

* Drop Rust library entirely
* Upgrade `commit-msg` hook to Gerrit 3.10.0
* Upgrade some dependencies, especially clap

## 4.0.0 / 2024-07-27

* Update dependencies, target modern Rust
* Drop git2 dependencies
* Use rustls instead of openssl
* Use single-threaded tokio

## 3.1.1 / 2021-04-01

* Make `grr rebase` actually work
* Use `tokio` 1.0
* Upgrade some dependencies

## 3.1.0 / 2021-01-21

* Allow setting defualt branch to something other than `master`
* Upgrade dependencies

## 3.0.0 / 2020-09-05

* Switch to async `reqwest`
* Use `anyhow` for error handling
* Upgrade dependencies

## 2.2.2 / 2020-07-15

* Add `--force` flag to `grr init`
* Update commit-msg hook from Gerrit 3.2
* Upgrade dependencies

## 2.2.1 /2020-06-30

* Support `--topic`, `--code-review`, `--verified`, `--submit`, and `--hashtags`
* Upgrade dependencies

## 2.2.0 / 2020-06-22

* Split some useful functions into a library
* Include rustc version in `--version`

## 2.1.0 / 2020-06-15

* Use `git2` for some operations
* Support `grr --help` and `grr --version`
* Fail more clearly if `chmod +x` fails
* Enable gzip compression for HTTP requests
* Upgrade dependencies

## 2.0.0 / 2020-06-10

* Initial Rust release
