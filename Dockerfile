FROM rust:latest AS builder

WORKDIR /srv/grr
COPY src src
COPY Cargo.toml .
COPY Cargo.lock .

RUN cargo install --path .

FROM debian:bookworm-slim
COPY --from=builder /usr/local/cargo/bin/grr /usr/local/bin/grr
ENTRYPOINT ["grr"]
CMD ["help"]
