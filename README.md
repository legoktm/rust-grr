grr
===
[![crates.io](https://img.shields.io/crates/v/gerrit-grr.svg)](https://crates.io/crates/gerrit-grr)
[![pipeline status](https://gitlab.com/legoktm/rust-grr/badges/master/pipeline.svg)](https://gitlab.com/legoktm/rust-grr/-/commits/master)
[![coverage report](https://gitlab.com/legoktm/rust-grr/badges/master/coverage.svg)](https://legoktm.gitlab.io/rust-grr/tarpaulin-report.html)

`grr` is a simple utility to make using Gerrit a little less painful.

The basic workflow involves using a detached head, pulling down changes from
gerrit to work on them, and re-submitting them. Inspired by [git-review](https://pypi.python.org/pypi/git-review),
`grr` reads from `.gitreview` files as needed.

Installation: `cargo install gerrit-grr`

A docker image is also available: `registry.gitlab.com/legoktm/rust-grr`

Linux binaries can be [downloaded from GitLab](https://gitlab.com/legoktm/rust-grr/-/tags).

## Usage

* `grr [branch]`: Shorthand for `grr review`
* `grr review [branch]`: Submits a patch for review against the specified branch (defaults to `master`)
* `grr fetch 12345[:2]`: Pulls change 12345. An optional patchset # can be specified, otherwise the latest will be used.
* `grr cherry-pick 12345[:2]`: Just like fetch, except it cherry-picks the patch on top of HEAD
* `grr pull [branch]`: Pulls the latest remote changes and checks it out (defaults to `master`)
* `grr checkout [branch]`: Checkout the given branch (defaults to `master`)
* `grr rebase [branch]`: Rebase on top of the given branch (defaults to `master`)
* `grr init`: Installs commit-msg hook

## Default branch
The current default branch is `master`, it may switch to `main` in the future. You can change the default branch on a
per-repository basis or systemwide by setting the `grr.defaultBranch` git config option.

```
# For a single repository
git config grr.defaultBranch main

# For all repositories, unless overridden in that repository
git config --global grr.defaultBranch main
```

## History
`grr` was [originally written in 2014](https://github.com/legoktm/grr) in Python. It was ported
to Rust in 2020.

## License
grr is (C) 2020-2021 Kunal Mehta, released under the GPLv3 or any later version, see COPYING for details.
